Tree parametrization
--------------------

This repository contains examples of Code_Saturne cases used in the study: Parameterizing the aerodynamic effect of trees in street canyons for the street-network model MUNICH using the CFD model Code_Saturne.

The three folders correspond to the three canyons simulated: a large canyon ([LC](/Large_canyon/)) of aspect ratio 0.3, an intermediate canyon ([IC](/Intermediate_Canyon/)) of aspect ratio 0.5 and a narrow canyon ([NC](/Narrow_Canyon/)) of aspect ratio 1.0.
Each folder contains the mesh (mesh_canyon.med), the source codes (cs_user*) and the set-up (setup.xml) used to performed the simulations.
/*============================================================================
 * This function is called at the end of each time step, and has a very
 *  general purpose
 *  (i.e. anything that does not have another dedicated user function)
 *============================================================================*/

/* Code_Saturne version 6.0-beta */

/*
  This file is part of Code_Saturne, a general-purpose CFD tool.

  Copyright (C) 1998-2019 EDF S.A.

  This program is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation; either version 2 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
  Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/*----------------------------------------------------------------------------*/

#include "cs_defs.h"
#include "cs_math.h"

/*----------------------------------------------------------------------------
 * Standard C library headers
 *----------------------------------------------------------------------------*/

#include <assert.h>
#include <math.h>

#if defined(HAVE_MPI)
#include <mpi.h>
#endif

/*----------------------------------------------------------------------------
 * PLE library headers
 *----------------------------------------------------------------------------*/

#include <ple_coupling.h>

/*----------------------------------------------------------------------------
 *  Local headers
 *----------------------------------------------------------------------------*/

#include "cs_headers.h"

/*----------------------------------------------------------------------------
 *  Header for the current file
 *----------------------------------------------------------------------------*/

#include "cs_prototypes.h"

/*----------------------------------------------------------------------------*/

BEGIN_C_DECLS

/*----------------------------------------------------------------------------*/
/*!
 * \file cs_user_extra_operations.c
 *
 * \brief This function is called at the end of each time step, and has a very
 * general purpose (i.e. anything that does not have another dedicated
 * user function)
 */
/*----------------------------------------------------------------------------*/

/*============================================================================
 * User function definitions
 *============================================================================*/

/*----------------------------------------------------------------------------*/
/*!
 * \brief This function is called at the end of each time step.
 *
 * It has a very general purpose, although it is recommended to handle
 * mainly postprocessing or data-extraction type operations.
 *
 * \param[in, out]  domain   pointer to a cs_domain_t structure
 */
/*----------------------------------------------------------------------------*/

void
cs_user_extra_operations(cs_domain_t     *domain)
{
  /* Get number of cells */
  const cs_mesh_t *m = domain->mesh;
  const cs_lnum_t n_cells = m->n_cells;

  /* Get physical fields */
  const cs_real_t *prop = cs_field_by_name("traceur")->val;
  const cs_real_t *proptv = cs_field_by_name("traceurv")->val;
  const cs_real_t *densite = cs_field_by_name("density")->val;
  const cs_real_3_t *vel = cs_field_by_name("velocity")->val;
  const cs_real_t *k = cs_field_by_name("k")->val;
  const cs_real_t *epsilon = cs_field_by_name("epsilon")->val;

  /* Name of the first zone and first output file */
  const char zone[100] = "x>27.5 and x<55 and z>0 and z<8.5";
  const char fname[100] = "concentration_traceur_rue.dat";
  /* Name of the second zone and output file */
  char zone2[100] = "x>27.5 and x<55. and z>8.5 and z<17.";
  char fname2[100] = "concentration_haut_rue.dat";

  /* Write header at the first time step */
  FILE *f = NULL;
  FILE *f2 = NULL;
  if (cs_glob_time_step->nt_cur == cs_glob_time_step->nt_ini && cs_glob_rank_id <= 0) {
    f = fopen(fname,"w");
    fprintf(f, "#%17s%100s\n", "zone : ", zone);
    fprintf(f, "#%s\n", "temps (sec), traceur (microg/m3), traceurv (microg/m3), Vx, Vy, Vz, densité (kg/m3), k, epsilon");
    fclose(f);
    f2 = fopen(fname2,"w");                                                                      
    fprintf(f2, "#%17s%100s\n", "zone : ", zone2);                                               
    fprintf(f2, "#%s\n", "temps (sec), traceur (microg/m3), traceurv (microg/m3), Vx, Vy, Vz, densité (kg/m3), k, epsilon");
    fclose(f2);
  }

  /* Open file and print time */
  if (cs_glob_rank_id <= 0) {
    f = fopen(fname,"a");
    fprintf(f, "%17.9e ", cs_glob_time_step->t_cur);
    f2 = fopen(fname2,"a");
    fprintf(f2, "%17.9e ", cs_glob_time_step->t_cur);
  }

  /* Allocate memory */
  cs_lnum_t n_cells_sel = 0;
  cs_lnum_t *cells_sel_ids = NULL;
  BFT_MALLOC(cells_sel_ids, m->n_cells, cs_lnum_t);

  /* Temporary variables */
  cs_real_t sum, sumtv, rrho, sumx, sumy, sumz, sumk, sumepsilon, sumvolume;

  /* Get cells for the first zone */
  cs_selector_get_cell_list(zone, &n_cells_sel, cells_sel_ids);

  /* Average various quantities over the first zone */
  {
    sum = 0;
		sumtv = 0;
    rrho = 0;
    sumx = 0;
    sumy = 0;
    sumz = 0;
    sumk = 0;
    sumepsilon = 0;
    sumvolume = 0;
    const cs_real_t *restrict volume = domain->mesh_quantities->cell_f_vol;
    for (cs_lnum_t iel = 0; iel < n_cells_sel; iel++) {
      const cs_lnum_t cell_id = cells_sel_ids[iel];
      sum += 1000. * prop[cell_id] * densite[cell_id] * volume[cell_id];
      sumtv += 1000. * proptv[cell_id] * densite[cell_id] * volume[cell_id];
      rrho += densite[cell_id] * volume[cell_id];
      sumx += vel[cell_id][0] * volume[cell_id];
      sumy += vel[cell_id][1] * volume[cell_id];
      sumz += vel[cell_id][2] * volume[cell_id];
      sumk += k[cell_id] * volume[cell_id];
      sumepsilon += epsilon[cell_id] * volume[cell_id];
      sumvolume += volume[cell_id];
    }
  }

  /* Parallel sum then write to file */
  cs_parall_sum(1, CS_DOUBLE, &sum);
  cs_parall_sum(1, CS_DOUBLE, &sumtv);
  cs_parall_sum(1, CS_DOUBLE, &rrho);
  cs_parall_sum(1, CS_DOUBLE, &sumx);
  cs_parall_sum(1, CS_DOUBLE, &sumy);
  cs_parall_sum(1, CS_DOUBLE, &sumz);
  cs_parall_sum(1, CS_DOUBLE, &sumvolume);
  cs_parall_sum(1, CS_DOUBLE, &sumk);
  cs_parall_sum(1, CS_DOUBLE, &sumepsilon);
  if (cs_glob_rank_id <= 0) fprintf(f," %17.9e", sum/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f," %17.9e", sumtv/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f," %17.9e", sumx/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f," %17.9e", sumy/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f," %17.9e", sumz/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f," %17.9e", rrho/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f," %17.9e", sumk/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f," %17.9e", sumepsilon/sumvolume);

  /* Print end of line and close the file */
  if (cs_glob_rank_id <= 0) {
    fprintf(f, "\n");
    fclose(f);
  }

  /* Get cells for the second zone */
  cs_selector_get_cell_list(zone2, &n_cells_sel, cells_sel_ids);

  /* Average various quantities over the second zone */
  {
    sum = 0;
		sumtv = 0;
    rrho = 0;
    sumx = 0;
    sumy = 0;
    sumz = 0;
    sumk = 0;
    sumepsilon = 0;
    sumvolume = 0;
    const cs_real_t *restrict volume = domain->mesh_quantities->cell_f_vol;
    for (cs_lnum_t iel = 0; iel < n_cells_sel; iel++) {
      const cs_lnum_t cell_id = cells_sel_ids[iel];
      sum += 1000. * prop[cell_id] * densite[cell_id] * volume[cell_id];
      sumtv += 1000. * proptv[cell_id] * densite[cell_id] * volume[cell_id];
      rrho += densite[cell_id] * volume[cell_id];
      sumx += vel[cell_id][0] * volume[cell_id];
      sumy += vel[cell_id][1] * volume[cell_id];
      sumz += vel[cell_id][2] * volume[cell_id];
      sumk += k[cell_id] * volume[cell_id];
      sumepsilon += epsilon[cell_id] * volume[cell_id];
      sumvolume += volume[cell_id];
    }
  }

  /* Parallel sum then write to file */
  cs_parall_sum(1, CS_DOUBLE, &sum);
  cs_parall_sum(1, CS_DOUBLE, &sumtv);
  cs_parall_sum(1, CS_DOUBLE, &rrho);
  cs_parall_sum(1, CS_DOUBLE, &sumx);
  cs_parall_sum(1, CS_DOUBLE, &sumy);
  cs_parall_sum(1, CS_DOUBLE, &sumz);
  cs_parall_sum(1, CS_DOUBLE, &sumvolume);
  cs_parall_sum(1, CS_DOUBLE, &sumk);
  cs_parall_sum(1, CS_DOUBLE, &sumepsilon);
  if (cs_glob_rank_id <= 0) fprintf(f2," %17.9e", sum/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f2," %17.9e", sumtv/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f2," %17.9e", sumx/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f2," %17.9e", sumy/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f2," %17.9e", sumz/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f2," %17.9e", rrho/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f2," %17.9e", sumk/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f2," %17.9e", sumepsilon/sumvolume);
  if (cs_glob_rank_id <= 0) fprintf(f2," %17.9e", sumvolume);

  /* Print end of line and close the file */
  if (cs_glob_rank_id <= 0) {
    fprintf(f2, "\n");
    fclose(f2);
  }

  /* Free memory */
  BFT_FREE(cells_sel_ids);

}

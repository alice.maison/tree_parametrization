!-------------------------------------------------------------------------------

!                      Code_Saturne version 6.1-alpha
!                      --------------------------
! This file is part of Code_Saturne, a general-purpose CFD tool.
!
! Copyright (C) 1998-2019 EDF S.A.
!
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
! details.
!
! You should have received a copy of the GNU General Public License along with
! this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
! Street, Fifth Floor, Boston, MA 02110-1301, USA.

!-------------------------------------------------------------------------------

!===============================================================================
! Purpose:
! -------

!> \file cs_user_initialization.f90
!>
!> \brief Initialize variables
!>
!> See \subpage cs_user_initialization for examples.
!>
!> This subroutine is called at beginning of the computation
!> (restart or not) before the loop time step.
!>
!> This subroutine enables to initialize or modify (for restart)
!> unkown variables and time step values.
!>
!> Modification of the behaviour law of physical quantities (rom, viscl,
!> viscls, cp) is not done here. It is the purpose of the user subroutine
!> \ref cs_user_physical_properties
!>
!> \c rom and \c viscl values are equal to \c ro0 and \c viscl0 or initialize
!> by reading the restart file.
!> Variables diffusivity and specific heat (when they are defined) have no value
!> except if they are read from a restart file.
!>
!> \par cs_user_initialization_cell_id Cells identification
!>
!> Cells may be identified using the \ref getcel subroutine.
!> The syntax of this subroutine is described in the
!> \ref cs_user_boundary_conditions subroutine,
!> but a more thorough description can be found in the user guide.
!>
!> Field cell values arrays can be retrieved using the appropriate access
!> functions as described \ref field "here".
!>
!> Example of field ids:
!> - Density:                        \c irom
!> - Dynamic molecular viscosity:    \c iviscl
!> - Turbulent viscosity:            \c ivisct
!> - Specific heat:                  \c icp
!> - Diffusivity(lambda):            \c field_get_key_int(ivarfl(isca(iscal)),
!>                                      kivisl, ...)
!>
!
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Arguments
!______________________________________________________________________________.
!  mode           name          role                                           !
!______________________________________________________________________________!
!> \param[in]     nvar          total number of variables
!> \param[in]     nscal         total number of scalars
!> \param[in]     dt            time step (per cell)
!_______________________________________________________________________________

subroutine cs_user_f_initialization &
 ( nvar   , nscal  ,                                              &
   dt     )

!===============================================================================

!===============================================================================
! Module files
!===============================================================================

use paramx
use pointe
use numvar
use optcal
use cstphy
use cstnum
use entsor
use parall
use period
use ppppar
use ppthch
use coincl
use cpincl
use ppincl
use atincl
use ctincl
use ppcpfu
use cs_coal_incl
use cs_fuel_incl
use mesh
use field
use turbomachinery
use cs_c_bindings
use user_module ! initialize_chem_source_term

!===============================================================================

implicit none

! Arguments
integer :: nvar, nscal
double precision dt(ncelet)

! Local variables
! Meca
logical :: meca_arbre, meca_voiture
double precision dp_meca_arbre, dp_meca_voiture
! Chemistry
logical :: depot_arbre, emission_arbre, emission_voiture
double precision chimie_voiture, chimie_arbre_em, chimie_arbre_de

!===============================================================================

dp_meca_voiture = notebook_parameter_value_by_name("meca_voiture")
if (abs(dp_meca_voiture - 0) .lt. epzero) then
   meca_voiture = .false.
else
   meca_voiture = .true.
endif

dp_meca_arbre = notebook_parameter_value_by_name("meca_arbre")
if (abs(dp_meca_arbre - 0) .lt. epzero) then
   meca_arbre = .false.
else
   meca_arbre = .true.
endif

chimie_voiture = notebook_parameter_value_by_name("chimie_voiture")
if (abs(chimie_voiture - 0) .lt. epzero) then
   emission_voiture = .false.
else
   emission_voiture = .true.
endif

chimie_arbre_em = notebook_parameter_value_by_name("chimie_arbre_em")
if (abs(chimie_arbre_em - 0) .lt. epzero) then
   emission_arbre = .false.
else
   emission_arbre = .true.
endif

chimie_arbre_de = notebook_parameter_value_by_name("chimie_arbre_de")
if (abs(chimie_arbre_de - 0) .lt. epzero) then
   depot_arbre = .false.
else
   depot_arbre = .true.
endif

call initialize_chem_source_term(meca_voiture, meca_arbre, emission_voiture, emission_arbre, depot_arbre)

return
end subroutine cs_user_f_initialization
